import { Component, OnInit } from '@angular/core';
import {CommentaireService} from "../service/commentaire.service";

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {
  listBlog;
  listcommentaire;
  listusers;
  description;
  iduser;
  affichemodif = false;
  affiaddcomment = false;
  idcommentaire;
  description1;

  constructor(private comment: CommentaireService) {
  }

  ngOnInit(): void {
    this.allArticle();
    this.allUser();
  }

  recuper(description1, id) {
    this.affichemodif = true;
    this.description = description1;
    this.idcommentaire = id;
    console.log(description1);
  }

  allUser() {
    this.comment.all().subscribe(result => {
        console.log(result);
        this.listusers = result
      }
    )
  }

  allArticle() {
    this.comment.AllArticle().subscribe(result => {
        console.log(result);
        this.listBlog = result
      }
    )
  }

  affichadd() {
    this.affiaddcomment = true;
  }

  addComment(idart) {
    console.log(this.iduser)
    this.comment.addComment(idart, this.iduser, {description: this.description}).subscribe(res => {
      console.log(res);
      this.allArticle();
      this.description = '';
    })
  }

  deleteComment(id) {
    this.comment.deleteComment(id).subscribe(result => {
      console.log(result);
      this.allArticle()
    })
  }

  editComment() {
    this.comment.ModifComment(this.idcommentaire, {description: this.description}).subscribe(result => {
      console.log(result);
      this.allArticle();
      this.description = '';
    })

  }


}

